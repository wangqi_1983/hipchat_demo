Main logic:
1. RegexParser to define the regex function with mention, emoticon and url pattern.
2. ElementBuilder to extract and build those three types of elements.
3. Util to generate the JSON object and JSON string.
4. UI stuffs are all in ViewController to demo input strings and extract JSON string.

Open Source Libraries(use cocoaPods):
1. SwiftyJSON to generate JSON object from dictionary and easily get the string for JSON object, this library especially useful for RESTful service handling.
2. ReadabilityKit to generate the title for specific URL.
3. MBAutoGrowingTextView is an automatically growing textView as most of the chatting apps have.

Unit Tests:
All testcases are in "hipChatDemoTests.swift", first do some specific functional testing for all three types of elements, then finished 4 samples giving for this project.