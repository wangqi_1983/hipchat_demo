//
//  ElementBuilder.swift
//  hipChatDemo
//
//  Created by Qi Wang on 11/08/2016.
//  Copyright © 2016 None. All rights reserved.
//

import Foundation
import ReadabilityKit
import SwiftyJSON

enum ElementType {
    case Mention
    case Emoticon
    case URL
    
    var pattern: String {
        switch self
        {
        case .Mention:
            return RegexParser.mentionPattern
        case .Emoticon:
            return RegexParser.emoticonPattern
        case .URL:
            return RegexParser.urlPattern
        }
    }
    
    var jsonKeyName: String {
        switch self
        {
        case .Mention:
            return "mentions"
        case .Emoticon:
            return "emoticons"
        case .URL:
            return "links"
        }
    }
}

struct ElementBuilder {
    static func createElements(type: ElementType, text: String, range: NSRange) -> [AnyObject]
    {
        let matches = RegexParser.getElements(from: text, with: type.pattern, range: range)
        let nsstring = text as NSString
        var elements = [AnyObject]()
        
        for match in matches where match.range.length > 2
        {
            var range: NSRange
            var word: String
            var linkDict: [String: String]
            switch type
            {
            case .Mention:
                range = NSRange(location: match.range.location + 1, length: match.range.length - 1)
                word = nsstring.substringWithRange(range)
                if word.hasPrefix("@")
                {
                    word.removeAtIndex(word.startIndex)
                }
                elements.append(word)
            case .Emoticon:
                range = NSRange(location: match.range.location + 1, length: match.range.length-2)
                word = nsstring.substringWithRange(range)
                elements.append(word)
            case .URL:
                word = nsstring.substringWithRange(match.range)
                    .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                let parser = Readability(url: NSURL(string: word)!)
                let title = parser.title() ?? ""
                linkDict = ["url": word, "title": title]
                elements.append(linkDict)
            }
        }
        
        return elements
    }    
}