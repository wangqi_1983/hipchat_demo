//
//  RegexParser.swift
//  hipChatDemo
//
//  Created by Qi Wang on 10/08/2016.
//  Copyright © 2016 None. All rights reserved.
//

import Foundation

struct RegexParser {
    static let mentionPattern = "(?:^|[^a-zA-Z0-9_＠!@#$%&*])(?:(?:@|＠)(?!\\/))([a-zA-Z0-9_.]{1,15})(?:\\b(?!@|＠)|$)"
    static let emoticonPattern = "\\([a-zA-Z0-9\\-\\/]{1,15}\\)"
    static let urlPattern = "((https?://|www\\.|pic\\.)[-\\w;/?:@&=+$\\|\\_.!~*\\|'()\\[\\]%#,☺]+[\\w/#](\\(\\))?)" + "(?=$|[\\s',\\|\\(\\).:;?\\-\\[\\]>\\)])"
    
    static func getElements(from text: String, with pattern: String, range: NSRange) -> [NSTextCheckingResult]{
        guard let elementRegex = try? NSRegularExpression(pattern: pattern, options: [.CaseInsensitive]) else { return [] }
        return elementRegex.matchesInString(text, options: [], range: range)
    }
}