//
//  ViewController.swift
//  hipChatDemo
//
//  Created by Qi Wang on 10/08/2016.
//  Copyright © 2016 None. All rights reserved.
//

import UIKit
import MBAutoGrowingTextView

class ViewController: UIViewController {

    @IBOutlet weak var inputTextBar: MBAutoGrowingTextView!
    @IBOutlet weak var resultTextView: UITextView!
    @IBOutlet weak var inputTextLabel: UILabel!
    
    @IBAction func extractJSON(sender: AnyObject) {
        let jsonString = Util.generateJSONString(inputTextBar.text)
        inputTextLabel.text = inputTextBar.text
        inputTextBar.text = ""
        resultTextView.text = jsonString ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}

extension ViewController: UITextViewDelegate{
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        UIView.beginAnimations("ResizeForKeyboard", context: nil)
        UIView.setAnimationDuration(0.3)
        let width = view.frame.size.width
        let height = view.frame.size.height
        let frame = CGRectMake(0, -260, width, height)
        view.frame = frame
        UIView.commitAnimations()
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView)
    {
        view.frame = CGRectMake(0, 0, view.frame.width, view.frame.height)
    }
}

