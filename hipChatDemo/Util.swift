//
//  Util.swift
//  hipChatDemo
//
//  Created by Qi Wang on 10/08/2016.
//  Copyright © 2016 None. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct Util {
    
    static func generateJSONString(textString: String!) -> String?
    {
        let jsonVal = generateJSON(textString)
        
        if jsonVal == nil
        {
            return nil
        }
        
        // Since based on the JSON spec indicates that the forward slash can optionally be escaped
        let jsonString = jsonVal?.description.stringByReplacingOccurrencesOfString("\\/", withString: "/")
        
        return jsonString
    }
    
    static func generateJSON(textString: String!) -> JSON?
    {
        if textString == nil
        {
            return nil
        }
        
        var retDict = [String: AnyObject]()
        let textLength = textString.utf16.count
        let textRange = NSRange(location: 0, length: textLength)
        
        let mentionElements = ElementBuilder.createElements(.Mention, text: textString, range: textRange)
        if mentionElements.count != 0
        {
            retDict[ElementType.Mention.jsonKeyName] = mentionElements
        }
        
        let emoticonElements = ElementBuilder.createElements(.Emoticon, text: textString, range: textRange)
        if emoticonElements.count != 0
        {
            retDict[ElementType.Emoticon.jsonKeyName] = emoticonElements
        }
        
        let linkElements = ElementBuilder.createElements(.URL, text: textString, range: textRange)
        if linkElements.count != 0
        {
            retDict[ElementType.URL.jsonKeyName] = linkElements
        }
        
        if retDict.isEmpty
        {
            return nil
        }
        
        let jsonVal = JSON(retDict)
//        print(jsonVal.description)
        
        return jsonVal
    }
}
