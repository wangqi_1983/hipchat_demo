//
//  hipChatDemoTests.swift
//  hipChatDemoTests
//
//  Created by Qi Wang on 10/08/2016.
//  Copyright © 2016 None. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import hipChatDemo

class hipChatDemoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInvalid()
    {
        var jsonVal = Util.generateJSON(nil)
        XCTAssertNil(jsonVal)
        
        var testString = ""
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
        
        testString = "This is a beautiful world!"
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
        
        testString = "😁"
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
    }
    
    func testMentions()
    {
        var testString = "@qi.wang"
        var jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.Mention.jsonKeyName][0], "qi.wang")
        
        testString = "@qi_wang"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.Mention.jsonKeyName][0], "qi_wang")
        
        testString = "@_wang"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.Mention.jsonKeyName][0], "_wang")
        
        testString = "@wangqi@blade"
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
    }
    
    func testEmoticons()
    {
        var testString = "(h)"
        var jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.Emoticon.jsonKeyName][0], "h")
        
        testString = "(helloworld)"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.Emoticon.jsonKeyName][0], "helloworld")
        
        // 15 characters
        testString = "(qwertyuiopasdfg)"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.Emoticon.jsonKeyName][0], "qwertyuiopasdfg")
        
        // Longer than 15
        testString = "(qwertyuiopasdfgl)"
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
        
        testString = "(hello world)"
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
    }
    
    func testLinks()
    {
        var testString = "http://www.atlassian.com"
        var jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["url"], "http://www.atlassian.com")
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["title"], "Software Development and Collaboration Tools | Atlassian")
        
        testString = "https://www.atlassian.com"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["url"], "https://www.atlassian.com")
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["title"], "Software Development and Collaboration Tools | Atlassian")
        
        testString = "www.atlassian.com"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["url"], "www.atlassian.com")
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["title"], "")
        
        testString = "atlassian.com"
        jsonVal = Util.generateJSON(testString)
        XCTAssertNil(jsonVal)
        
        testString = "www.hellohellohellohellohello.com"
        jsonVal = Util.generateJSON(testString)
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["url"], "www.hellohellohellohellohello.com")
        XCTAssertEqual(jsonVal![ElementType.URL.jsonKeyName][0]["title"], "")
    }
    
    func testSample1_hipChat()
    {
        let testString = "@chris you around?"
        let generatedJSON = Util.generateJSON(testString)
        
        XCTAssertNotNil(generatedJSON)
        XCTAssertNotNil(generatedJSON![ElementType.Mention.jsonKeyName])
        XCTAssertEqual(generatedJSON![ElementType.Mention.jsonKeyName].count, 1)
        XCTAssertEqual(generatedJSON![ElementType.Mention.jsonKeyName][0], "chris")
    }
    
    func testSample2_hipChat()
    {
        let testString = "Good morning! (megusta) (coffee)"
        let generatedJSON = Util.generateJSON(testString)
        
        XCTAssertNotNil(generatedJSON)
        XCTAssertNotNil(generatedJSON![ElementType.Emoticon.jsonKeyName])
        XCTAssertEqual(generatedJSON![ElementType.Emoticon.jsonKeyName].count, 2)
        XCTAssertEqual(generatedJSON![ElementType.Emoticon.jsonKeyName], ["megusta", "coffee"])
    }
    
    func testSample3_hipChat()
    {
        let testString = "Olympics are starting soon;http://www.nbcolympics.com"
        let generatedJSON = Util.generateJSON(testString)
        
        XCTAssertNotNil(generatedJSON)
        XCTAssertNotNil(generatedJSON![ElementType.URL.jsonKeyName])
        XCTAssertEqual(generatedJSON![ElementType.URL.jsonKeyName].count, 1)
        XCTAssertEqual(generatedJSON![ElementType.URL.jsonKeyName][0]["url"], "http://www.nbcolympics.com")
        XCTAssertEqual(generatedJSON![ElementType.URL.jsonKeyName][0]["title"], "2016 Rio Olympic Games | NBC Olympics")
    }
    
    func testSample4_hipChat()
    {
        let testString = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
        let generatedJSON = Util.generateJSON(testString)
        
        XCTAssertNotNil(generatedJSON)
        XCTAssertEqual(generatedJSON![ElementType.Mention.jsonKeyName].count, 2)
        XCTAssertEqual(generatedJSON![ElementType.Mention.jsonKeyName], ["bob", "john"])
        
        XCTAssertEqual(generatedJSON![ElementType.Emoticon.jsonKeyName].count, 1)
        XCTAssertEqual(generatedJSON![ElementType.Emoticon.jsonKeyName][0], "success")
        
        XCTAssertEqual(generatedJSON![ElementType.URL.jsonKeyName].count, 1)
        XCTAssertEqual(generatedJSON![ElementType.URL.jsonKeyName][0]["url"], "https://twitter.com/jdorfman/status/430511497475670016")
        XCTAssertEqual(generatedJSON![ElementType.URL.jsonKeyName][0]["title"], "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\"")
    }
}
